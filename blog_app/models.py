from django.db import models
from django.urls import reverse

# Create your models here.

class Post(models.Model):
	title = models.CharField(max_length=200)
	author = models.ForeignKey('auth.User', on_delete=models.CASCADE,)
	body = models.TextField()

	def __str__(self):
		return self.title

	# Este método é utilizado em conjunto com CreateView e garante que após criado o novo post, sejamos redirecionados para a página 'post_detail'
	def get_absolute_url(self):
		return reverse('post_detail', args=[str(self.id)])
