from django.urls import path
from .views import HomeListView, BlogDetailView, BlogCreateView, BlogUpdateView, BlogDeleteView

urlpatterns = [
	path('', HomeListView.as_view(), name ='home'),
	path('post/<int:pk>/', BlogDetailView.as_view(), name='post_detail'),
	path('post/new_post/', BlogCreateView.as_view(), name='new_post'),
	path('post/<int:pk>/edit_post/', BlogUpdateView.as_view(), name='post_edit'),
	path('post/<int:pk>/delete_post/', BlogDeleteView.as_view(), name='delete_post'),
]