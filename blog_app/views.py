from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Post

# Create your views here.

class HomeListView(ListView):
	model = Post
	context_object_name = 'posts'
	template_name = 'home.html'


class BlogDetailView(DetailView):
	model = Post
	context_object_name = 'post'
	template_name = 'post_detail.html'


class BlogCreateView(CreateView):
	model = Post
	template_name = 'new_post.html'
	fields = '__all__'


class BlogUpdateView(UpdateView):
	model = Post
	# context_object_name = 'post'
	template_name = 'post_edit.html'
	fields = ['title', 'body']


class BlogDeleteView(DeleteView):
	model = Post
	context_object_name = 'post'
	template_name = 'delete_post.html'
	success_url = reverse_lazy('home')
		
		
		